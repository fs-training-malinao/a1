// Rendering

import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

// 2
import 'bootstrap/dist/css/bootstrap.min.css'


const root = ReactDOM.createRoot(document.getElementById('root'));
//this code is connected to public >> index.html

root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
  /* React Stric Mode is to not render the file if have error and display error message instead */
  
);


/*
  [SECTION] JSX (JavaScript + XML)
  - is an extension of JS that let us create objects which then be compiled and added as HTML elements.
  - With JSX, we are able to create HTML elements using JS.

  .value // form elements
  .inner HTML // div, p, span

  JSX provides 'synatactic sugar' for creating react components.
    - syntax within a programming language that is designed to make things easier to read or to express.
    <Home />


*/

// 1 JSX Format 
// const name = "Christopher Malinao";
// const element = <h1> Hello, {name} </h1>
// root.render(element);


