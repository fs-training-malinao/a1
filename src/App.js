// Parent component

import './App.css';
import {BrowserRouter as Router, Routes, Route} from "react-router-dom";

import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Profile from './pages/Profile';



function App() {

  // 7
   let user = {
              firstName: "John",
              lastName: "Doe",
              email: "john.doe@mail.com",
              mobileNo: "09123456789",
              isAdmin: false

        }

  return (
    <Router> {/*Fragment <> For components will not be overwrite by other components </>*/}
      <AppNavbar/>
      
      <Routes>
        <Route exact path="/" element={<Home />} />
        <Route exact path="/profile" element=<Profile data={user}/> />
      </Routes>
    </Router>
  );
}

export default App;

// Rendering process of calling or invoking our function
// Mounting is to display into the webpage 