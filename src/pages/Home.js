// 6
import Banner from "../components/Banner";

export default function Home(){
	// 7
	const bannerProp = {
		title: "Zuitt Coding Bootcamp",
		content: "Opportunities for everyone, everywhere",
		destination: "",
		label: "Enroll"
	}

	return(
		<>
			
			<Banner data={bannerProp}/>
			
		</>
	)
}