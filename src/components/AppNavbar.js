import { Nav, Navbar, Container } from "react-bootstrap"

export default function AppNavbar(){


	//function based components
	return(
		// What is inside the return will be rendered.
		<Navbar bg="light" expand="lg">
	{/*These tags that are in Title Case are react-boots*/}
		<Container>
			<Navbar.Brand href="#home">Zuitt</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				{/*className is use instead class, to specify a CSS class*/}
				<Nav className="ms-auto">
					<Nav.Link href="#home">Home</Nav.Link>
					<Nav.Link href="#courses">Courses</Nav.Link>
				</Nav>
			</Navbar.Collapse>
		</Container>
		</Navbar>



		)

}