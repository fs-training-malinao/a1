import {Row, Col, Button} from "react-bootstrap"

/*

Pass-by-value (parameter passing)

	let name = "John Doe"
	function fullName(myName){
		myName = "Chris Cross"
		console.log(`Hello my name is ${myName}`);
	}

	fullName(name);

	
Pass-by-reference(object, array)
let arrOfNum = [1, 2, 3, 4, 5]
arrOfNum[0] = 10;
console.log(arraOfNum[0]);


	
*/


// 3
export default function Banner({data}){
							
// prop means property
// Passing value from different component 
// Inherit - Parent to child
	
	// 5 Destucture
	const {title, content, label} = data;
	

	return(
		// 4
		<Row>
			<Col className="p-5 m-5 text-center">
				  	<h1>{title}</h1>
				  	<p>{content}</p>
				  	<Button>{label}</Button>
			</Col>
		</Row>

	)

}